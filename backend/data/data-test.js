// Example data
module.exports.scheduleSets = [
  {
    'id': 1,
    'name': 'ScheduleSet 1',
    'schedules': [
      { 'scheduleId': 4523, 'day': 'SUN', 'start': 15, 'end': 1425 },
      { 'scheduleId': 4524, 'day': 'SAT', 'start': 15, 'end': 1425 },
      { 'scheduleId': 4525, 'day': 'FRI', 'start': 15, 'end': 1425 },
      { 'scheduleId': 4526, 'day': 'THU', 'start': 15, 'end': 1425 },
      { 'scheduleId': 4527, 'day': 'WED', 'start': 15, 'end': 1425 },
      { 'scheduleId': 4528, 'day': 'TUE', 'start': 15, 'end': 1425 },
      { 'scheduleId': 4529, 'day': 'MON', 'start': 15, 'end': 660 },
      { 'scheduleId': 4530, 'day': 'MON', 'start': 780, 'end': 900 }
    ]
  },
  {
    'id': 2,
    'name': 'ScheduleSet 2',
    'schedules': [
      { 'scheduleId': 4531, 'day': 'SUN', 'start': 30, 'end': 1410 },
      { 'scheduleId': 4532, 'day': 'SAT', 'start': 30, 'end': 1410 },
      { 'scheduleId': 4533, 'day': 'FRI', 'start': 30, 'end': 1410 },
      { 'scheduleId': 4534, 'day': 'THU', 'start': 30, 'end': 1410 },
      { 'scheduleId': 4535, 'day': 'WED', 'start': 30, 'end': 1410 },
      { 'scheduleId': 4536, 'day': 'TUE', 'start': 30, 'end': 1410 },
      { 'scheduleId': 4537, 'day': 'MON', 'start': 30, 'end': 900 }
    ]
  },
  {
    'id': 3,
    'name': 'ScheduleSet 3',
    'schedules': [
      { 'scheduleId': 4539, 'day': 'SAT', 'start': 60, 'end': 1395 },
      { 'scheduleId': 4540, 'day': 'FRI', 'start': 60, 'end': 1395 },
      { 'scheduleId': 4541, 'day': 'THU', 'start': 60, 'end': 1395 },
      { 'scheduleId': 4542, 'day': 'WED', 'start': 60, 'end': 1395 },
      { 'scheduleId': 4543, 'day': 'TUE', 'start': 60, 'end': 1395 }
    ]
  }
]
