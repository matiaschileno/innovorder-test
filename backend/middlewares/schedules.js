const _ = require('lodash')
const getMinutes = require('date-fns/get_minutes')
const getHours = require('date-fns/get_hours')
const getSeconds = require('date-fns/get_seconds')
const getDay = require('date-fns/get_day')
const addMinutes = require('date-fns/add_minutes')
const Ajv = require('ajv')

const utils = require('../utils/nextOrderingDateUtils')
const data = require('../data/data-test')
// json schema for schedule update
const schema = require('../schemas/scheduleEdit.json')

const ajv = new Ajv({ allErrors: true })
const validate = ajv.compile(schema)

module.exports = {
  // READ Methods ----------------------------------------------------------------------------------------------------------
  getNextOrdering: async (req, res) => {
    // calculate next available ordering date
    let dateNextOrdering = new Date()

    const preparationDelay = 20
    const rushDelay = 10
    dateNextOrdering = addMinutes(dateNextOrdering, preparationDelay + rushDelay)
    let minutesNextOrdering = getMinutes(dateNextOrdering)
    let secondsNextOrdering = getSeconds(dateNextOrdering)

    // if minutesNextOrdering isn't modulo 15, modify date to the nearest time where modulo 15 is 0
    // or if minutesNextOrdering is modulo 15 but secondsNextOrdering is greater than 0
    if (minutesNextOrdering % 15 !== 0 || (minutesNextOrdering % 15 === 0 && secondsNextOrdering > 0)) {
      let newIntPart = Math.floor(minutesNextOrdering / 15) + 1
      let newMinutes = newIntPart * 15
      dateNextOrdering = addMinutes(dateNextOrdering, newMinutes - minutesNextOrdering)
      minutesNextOrdering = getMinutes(dateNextOrdering)
    }

    let hoursNextOrdering = getHours(dateNextOrdering)

    // check date schedule to validate chosen date
    let today = utils.weekday[getDay(dateNextOrdering)]

    const scheduleSet = _.find(data.scheduleSets, { 'id': parseInt(req.params.id) })
    const schedulesToday = _.filter(scheduleSet.schedules, { 'day': today })
    let isOk = false
    if (!_.isEmpty(schedulesToday)) {
      isOk = utils.validateNextOrderingDate(hoursNextOrdering, minutesNextOrdering, schedulesToday)
    }

    // if there are no today schedules or if chosen next available ordering date is not possible in today schedules
    // search for the soonest one
    if (!isOk) {
      let success
      let nextDayName
      [success, hoursNextOrdering, minutesNextOrdering, nextDayName] = utils.getSoonestStart(schedulesToday, dateNextOrdering, hoursNextOrdering, minutesNextOrdering, scheduleSet)
      if (!success) {
        res.json({ success: false, message: 'There is no ScheduleSets registered.' })
      }
      if (nextDayName) {
        return res.json({
          hour: hoursNextOrdering,
          minutes: minutesNextOrdering,
          nextDayName: nextDayName
        })
      }
    }

    return res.json({
      hour: hoursNextOrdering,
      minutes: minutesNextOrdering
    })
  },

  getSets: (req, res) => {
    return res.json(data.scheduleSets)
  },

  getByID: (req, res) => {
    const scheduleSet = _.find(data.scheduleSets, { 'id': parseInt(req.params.id) })
    if (_.isEmpty(scheduleSet)) {
      return res.status(404).end()
    }
    return res.json(scheduleSet.schedules)
  },

  // DELETE Methods ----------------------------------------------------------------------------------------------------------
  deleteSetByID: async (req, res) => {
    _.remove(data.scheduleSets, { 'id': parseInt(req.params.id) })
    return res.json({ message: 'Ok' })
  },

  deleteByID: async (req, res) => {
    const scheduleSet = _.find(data.scheduleSets, { 'id': parseInt(req.params.setID) })
    _.remove(scheduleSet.schedules, { 'scheduleId': parseInt(req.params.id) })
    return res.json({ message: 'Ok' })
  },

  // UPDATE Methods ----------------------------------------------------------------------------------------------------------
  updateByID: async (req, res) => {
    // check json schema
    const valid = validate(req.body)
    if (!valid) {
      console.log(validate.errors)
      return res.status(406).json({ message: 'Invalid json schema' })
    }
    const scheduleSet = _.find(data.scheduleSets, { 'id': parseInt(req.params.setID) })

    // check if it exists schedule conflicts in this day
    // range of new times of schedule to be updated
    const rangeUpdate = _.range(parseInt(req.body.start), parseInt(req.body.end) + 1, 15)
    // get this day schedules without schedule to be updated
    const thisDaySchedules = _.filter(scheduleSet.schedules, (schedule) => {
      if (schedule.day === req.body.day && schedule.scheduleId !== parseInt(req.params.id)) { return true }
    })
    // if there is schedules in the same day, search for start and end times conflicts
    if (!_.isEmpty(thisDaySchedules)) {
      let scheduleConflictExists = _.find(thisDaySchedules, (schedule) => {
        // check using range between start and end times
        let rangeSchedule = _.range(schedule.start, schedule.end + 1, 15)
        if (!_.isEmpty(_.intersection(rangeSchedule, rangeUpdate))) {
          return true
        }
      })

      if (!_.isEmpty(scheduleConflictExists)) {
        console.log()
        return res.status(406).json({ message: 'Schedule conflict. Please verify start and end times at this day' })
      }
    }

    // if there is no schedules conflicts, update
    let schedule = _.find(scheduleSet.schedules, { 'scheduleId': parseInt(req.params.id) })
    _.merge(schedule, req.body)
    return res.json({ message: 'Ok' })
  }
}
