const _ = require('lodash')
const getDay = require('date-fns/get_day')

module.exports = {
  weekday: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
  // Search for the next date (the soonest, today or another day) to be set as the next available ordering date
  getSoonestStart: (schedulesToday, dateNextOrdering, hoursNextOrdering, minutesNextOrdering, scheduleSet) => {
    // check if next available ordering date is possible today later
    let soonestHour = 23
    let soonestMin = 59

    if (!_.isEmpty(schedulesToday)) {
      _.each(schedulesToday, (schedule) => {
        let [startHour, startMinutes] = module.exports.getRealTime(schedule)
        if (soonestHour > startHour) {
          soonestHour = startHour
          soonestMin = startMinutes
        } else if (soonestHour === startHour && soonestMin > startMinutes) {
          soonestMin = startMinutes
        }
      })

      if (soonestHour > hoursNextOrdering) {
        return [true, soonestHour, soonestMin, '']
      }

      if (soonestHour === hoursNextOrdering) {
        if (soonestMin > minutesNextOrdering) {
          return [true, soonestHour, soonestMin, '']
        }
      }
    }

    // otherwise, get soonestStart from next days
    let schedulesNext = []
    let daysFromToday = 1
    let nextNumDay = ''
    let next = ''
    while (_.isEmpty(schedulesNext)) {
      nextNumDay = getDay(dateNextOrdering) + daysFromToday > 6 ? 0 : getDay(dateNextOrdering) + daysFromToday
      next = module.exports.weekday[nextNumDay]
      schedulesNext = _.filter(scheduleSet.schedules, { 'day': next })
      daysFromToday++
      if (daysFromToday === 6) {
        return false
      }
    }
    soonestHour = 23
    soonestMin = 59

    _.each(schedulesNext, (schedule) => {
      let [startHour, startMinutes] = module.exports.getRealTime(schedule)
      if (soonestHour > startHour) {
        soonestHour = startHour
        soonestMin = startMinutes
      } else if (soonestHour === startHour && soonestMin > startMinutes) {
        soonestMin = startMinutes
      }
    })

    return [true, soonestHour, soonestMin, next]
  },
  // Parse minutesOfTheDay for startDate and endDate. Separate hour and minutes for each one
  getRealTime: (schedule) => {
    const startDateInHours = schedule.start / 60
    const startHour = Math.floor(startDateInHours)
    const startMinutes = (startDateInHours - startHour) * 60
    const endDateInHours = schedule.end / 60
    const endHour = Math.floor(endDateInHours)
    const endMinutes = (endDateInHours - endHour) * 60

    return [startHour, startMinutes, endHour, endMinutes]
  },

  // Verify if next available ordering date is possible in today schedules
  validateNextOrderingDate: (hoursNextOrdering, minutesNextOrdering, schedulesToday) => {
    return _.find(schedulesToday, (schedule) => {
      let [startHour, startMinutes, endHour, endMinutes] = module.exports.getRealTime(schedule)
      if (hoursNextOrdering > startHour && hoursNextOrdering < endHour) {
        return true
      }

      if (hoursNextOrdering === startHour && hoursNextOrdering === endHour) {
        if (minutesNextOrdering >= startMinutes && minutesNextOrdering <= endMinutes) {
          return true
        }
      }
      return false
    })
  }
}
