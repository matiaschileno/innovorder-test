const express = require('express')
const bodyParser = require('body-parser')
const router = express.Router()

// Middlewares
const schedules = require('./middlewares/schedules')

// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
router.use(bodyParser.json())

router.get('/', (req, res, next) => {
  res.send('Welcome to this API')
})

// Sequencies
router.get('/schedules/sets', schedules.getSets)
router.get('/schedules/:id', schedules.getByID)
router.get('/schedules/sets/:id/next-ordering', schedules.getNextOrdering)

router.patch('/schedules/:setID/:id', schedules.updateByID)

router.delete('/schedules/sets/:id', schedules.deleteSetByID)
router.delete('/schedules/:setID/:id', schedules.deleteByID)

module.exports = router
