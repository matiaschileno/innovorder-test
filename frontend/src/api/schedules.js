import axios from 'axios'

axios.defaults.baseURL = 'https://innovorder-test.plaza.ninja'

const URL_API = '/api'

export default {
	async getNextOrdering(id) {
		const response = await axios.get(`${URL_API}/schedules/sets/${id}/next-ordering`)
		return response.data
	},
	async getSets() {
		const response = await axios.get(`${URL_API}/schedules/sets`)
		return response.data
	},
	async getByID(id) {
		const response = await axios.get(`${URL_API}/schedules/${id}`)
		return response.data
	},
	async deleteScheduleSet(id) {
		return await axios.delete(`${URL_API}/schedules/sets/${id}`)
	},
	async deleteSchedule(setID, id) {
		return await axios.delete(`${URL_API}/schedules/${setID}/${id}`)
	},
	async updateSchedule(setID, id, body) {
		return await axios.patch(`${URL_API}/schedules/${setID}/${id}`, body)
	}
}
