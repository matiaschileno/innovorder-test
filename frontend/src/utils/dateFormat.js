export default {
    days: {
        SUN: "Sunday",
        MON: "Monday",
        TUE: "Tuesday",
        WED: "Wednesday",
        THU: "Thursday",
        FRI: "Friday",
        SAT: "Saturday"
    },
    getRealTime(hourInMinutes) {
        const dateInHours = hourInMinutes / 60;
        const hour = Math.floor(dateInHours);
        const minutes = (dateInHours - hour) * 60;
        return `${hour}:${this.getFormattedMinutes(minutes)}`;
    },
    getFormattedMinutes(minutes) {
        return minutes.toString().padStart(2, "0");
    },
    getHumanDay(codeDay) {
        return this.days[codeDay]
    },
    getMinutesOfDay(time) {
        let timeStrip = time.split(":");
        let hours = parseInt(timeStrip[0]);
        let minutes = parseInt(timeStrip[1]);
        return hours * 60 + minutes;
    },
}