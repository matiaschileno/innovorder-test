# Innovorder Test

This SPA and API were written in JavaScript using Vue.js and Node.js

It was deployed in Docker with docker-compose.

## Use it

This application can be tested through the following link : https://innovorder-test.plaza.ninja

## Test

Three ScheduleSets were included for this test. First, you have to choose one of them to see the information of each Schedule in the set. Then, you can : 

- Remove the entire ScheduleSet
- Modify one Schedule of the selected set
- Remove a Schedule of the selected set
- Select another ScheduleSet

Also, when choosing a ScheduleSet, you will see the next available ordering date on the top-right corner. This date will be requested to server every 15:01 minutes (three times to be sure to get the date even if a request or two fail). If the current day has no Schedule, the next available ordering date will be the first start date on the next day (with a Schedule).

If a Schedule is modified, the server will check for conflicts with another Schedule on the same day.

